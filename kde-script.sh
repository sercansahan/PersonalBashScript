#!/bin/bash
CWD=$PWD
mkdir -p /tmp/yay_install
cd /tmp/yay_install

sudo pacman -S reflector

reflector --latest 5 --fastest 5 --age 48 --sort rate --protocol https --save /etc/pacman.d/mirrorlist

sudo mkdir /etc/pacman.d/hooks
sudo touch /etc/pacman.d/hooks/mirrorupgrade.hook 
echo '[Trigger]
Operation = Upgrade
Type = Package
Target = pacman-mirrorlist

[Action]
Description = Updating pacman-mirrorlist with reflector and removing pacnew...
When = PostTransaction
Depends = reflector
Exec = /bin/sh -c "reflector --latest 10 --fastest 10 --age 48 --sort rate --protocol https --save /etc/pacman.d/mirrorlist; rm -f /etc/pacman.d/mirrorlist.pacnew"' | sudo tee /etc/pacman.d/hooks/mirrorupgrade.hook 

sudo pacman -S go --noconfirm --needed

if [ ! -n "$(pacman -Qs yay)" ]; then
    curl -o PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=yay
    makepkg PKGBUILD --install --needed
fi

cd ..
rm -rf yay_install
cd $CWD

sudo touch /etc/pacman.d/hooks/systemd-boot.hook
echo '[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Updating systemd-boot
When = PostTransaction
Exec = /usr/bin/bootctl update' | sudo tee /etc/pacman.d/hooks/systemd-boot.hook

sudo touch /etc/udev/rules.d/100-iphone-usbmuxd.rules
echo 'ENV{DEVTYPE}=="usb_device", ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="05ac", ATTR{idProduct}=="129[0-9abcef]", RUN+="/usr/bin/systemctl restart usbmuxd"
ENV{DEVTYPE}=="usb_device", ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="05ac", ATTR{idProduct}=="12a[0-9ab]", RUN+="/usr/bin/systemctl restart usbmuxd"
ENV{DEVTYPE}=="usb_device", ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="0fca", ATTR{idProduct}=="8014", RUN+="/usr/bin/systemctl restart usbmuxd"' | sudo tee /etc/udev/rules.d/100-iphone-usbmuxd.rules

sudo udevadm control --reload-rules

sudo pacman -S ark bluedevil breeze-gtk dolphin-plugins ffmpegthumbs filelight haveged gwenview kate kcalc kde-gtk-config kdeplasma-addons kdeconnect kdialog kinfocenter kpatience krename kscreen ksshaskpass kwallet-pam kwalletmanager latte-dock okular plasma-browser-integration plasma-desktop plasma-nm plasma-pa powerdevil pulseaudio-bluetooth sddm-kcm spectacle user-manager ttf-dejavu xdg-desktop-portal-kde xf86-input-synaptics yakuake --noconfirm --needed

sudo pacman -S adapta-gtk-theme adapta-kde bleachbit cups cups-pdf dhclient exfat-utils firefox firefox-developer-edition gnome-keyring hunspell-en_GB hunspell-en_US intel-ucode keepassxc kvantum-theme-adapta mesa-vdpau neofetch networkmanager-openvpn noto-fonts noto-fonts-emoji ntfs-3g openssh otf-ipafont p7zip powerline-fonts pptpclient telegram-desktop thunderbird tlp transmission-qt ttf-hack ttf-hanazono ttf-liberation ttf-opensans unrar vlc wget xdg-user-dirs xf86-input-synaptics xorg-xclock xorg-xinit xorg-twm xterm x264 --noconfirm --needed

sudo systemctl enable sddm.service
sudo systemctl enable org.cups.cupsd.service
sudo systemctl enable haveged.service
sudo pacman -Rnsc dhcpcd netctl s-nail vi
sudo pacman -Rns $(pacman -Qtdq)
sudo pacman -D $(pacman -Qgeq base base-devel) --asdeps
sudo pacman -D $(pacman -Qtdq) --asexplicit
sudo pacman -D systemd-sysvcompat --asexplicit
yay -S dropbox hunspell-tr pkgbrowser --noconfirm --needed
mkdir ~/.dropbox-dist
chmod 0500 ~/.dropbox-dist
